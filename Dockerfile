FROM python:3.6

# http://docs.gunicorn.org/en/latest/install.html
# If you are using Debian GNU/Linux and it is recommended that you use system 
# packages to install Gunicorn except maybe when you want to use different 
# versions of Gunicorn with virtualenv.
RUN apt-get update && apt-get install -y \
    postgresql-client

#    gunicorn3

COPY smsadminproto /app/smsadminproto/
COPY smsadminmodels /app/smsadminmodels/
COPY smsadminworkers /app/smsadminworkers/

RUN pip3 install --upgrade pip

RUN pip3 install greenlet gevent gunicorn[gevent]

# If the code is not in the image, I cannot have the requirements file
# and load the Python packages
WORKDIR /app/smsadminproto
RUN pip3 install -r requirements.txt

# requirements.txt is simply "-e .[test]" which implies to be in the same
# directory
WORKDIR /app/smsadminmodels
RUN pip3 install -r /app/smsadminmodels/requirements.txt

WORKDIR /app/smsadminworkers
RUN pip3 install -r /app/smsadminworkers/requirements.txt

RUN mkdir /app-boot
COPY boot/boot.sh /app-boot

WORKDIR /app/smsadminproto
# 
# ENV FLASK_APP /app/smsadminproto/smsadminproto/smsadminproto.py
# ENV FLASK_DEBUG 1
# 
# CMD ["/usr/local/bin/flask", "run", "-h", "0.0.0.0"]

CMD ["/app-boot/boot.sh"]

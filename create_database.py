"""
Create the SMSAdmin database.
"""

# import logging
import os
from smsadminmodels.base import SMSAdminBase

# Import model classes is nessary to feed SMSAdminBase.metadata.tables
import smsadminmodels.acquisition  # noqa : F401
import smsadminmodels.stream  # noqa : F401
import smsadminmodels.play  # noqa : F401
from sqlalchemy import create_engine


if __name__ == "__main__":
    # Activate SQLAlchemy logging
    # logging.basicConfig()
    # logging.getLogger('sqlalchemy').setLevel(logging.DEBUG)

    basedir = os.path.abspath(os.path.dirname(__file__))
    default_db_dir = os.getcwd()

    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "DATABASE_URL", 
        "postgresql://mydatabase:mypassword@localhost:5432/mydatabase"
    )

    # engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=True)
    # Remove echo=True when logging
    engine = create_engine(SQLALCHEMY_DATABASE_URI)

    SMSAdminBase.metadata.create_all(engine)

    print(f"{SQLALCHEMY_DATABASE_URI} is created.")

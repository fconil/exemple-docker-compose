#!/bin/bash

if [ -z "$DB_HOST" ]
then
    DB_HOST=postgres
fi

# wait-for-postgres
# https://docs.docker.com/compose/startup-order/
until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$DB_HOST" -U "$POSTGRES_USER" -c '\q'; do
    >&2 echo "Postgres is unavailable - sleeping"
    sleep 1
done

>&2 echo "Postgres is up - executing flask db cmd"

# Creates the migration repository
export DATABASE_URL="postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$DB_HOST:$DB_PORT/$POSTGRES_DB"

# Basic script, implement a better database creation script
python3 /app/smsadminmodels/scripts/create_database.py

# Start the worker
# flask run -h "0.0.0.0"

# Default port is 8000
gunicorn -w 4 -b 0.0.0.0:8000 wsgi
